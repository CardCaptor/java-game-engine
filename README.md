# Java Game Engine

This is just a proof of concept, in part to learn about game engines,
in part to have a visual playground fully customizable to learn more algorithms.

# Pre Requisites

- Maven
- JDK > v8

# How to Run

1 - Install dependencies

```bash
mvn clean install
```

2 - Compile

```bash
mvn clean package
```

3 - Run

This repo relies on [maven-exec-plugin](https://www.mojohaus.org/exec-maven-plugin/) to run

```bash
mvn exec:java
```

## License

GNU Licence
