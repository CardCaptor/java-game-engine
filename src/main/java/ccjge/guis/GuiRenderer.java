package ccjge.guis;

import java.util.List;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import ccjge.models.RawModel;
import ccjge.renderEngine.Loader;
import ccjge.utils.Maths;

public class GuiRenderer {

    private final RawModel quad;
    private GuiShader guiShader;

    public GuiRenderer(Loader loader) {
        float[] posiitions = { -1, 1, -1, -1, 1, 1, 1, -1 };
        quad = loader.loadToVAO(posiitions);
        guiShader = new GuiShader();
    }

    public void render(List<GuiTexture> guis) {
        guiShader.start();
        GL30.glBindVertexArray(quad.getVaoId());
        GL20.glEnableVertexAttribArray(0);
        for (GuiTexture gui : guis) {
            GL13.glActiveTexture(GL13.GL_TEXTURE0);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, gui.getTexture());
            Matrix4f transformationMatrix = Maths.createTransformationMatrix(gui.getPosition(), gui.getScale());
            guiShader.loadTransformationMatrix(transformationMatrix);
            GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, quad.getVertexCount());
        }
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
        guiShader.stop();
    }

    public void cleanUp() {
        guiShader.cleanUp();
    }
}
