package ccjge.guis;

import org.joml.Matrix4f;

import ccjge.shaders.ShaderProgram;

public class GuiShader extends ShaderProgram {

    private static final String VERTEX_SHADER_FILE = "src/main/java/ccjge/guis/guiVertexShader.vs";
    private static final String FRAGMENT_SHADER_FILE = "src/main/java/ccjge/guis/guiFragmentShader.fs";

    private int transformationMatrixLocation;

    public GuiShader() {
        super(VERTEX_SHADER_FILE, FRAGMENT_SHADER_FILE);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }

    @Override
    protected void getAllUniformLocations() {
        transformationMatrixLocation = super.getUniformLocation("transformationMatrix");
    }

    public void loadTransformationMatrix(Matrix4f transformationMatrix) {
        super.loadMatrix(transformationMatrixLocation, transformationMatrix);
    }

}
