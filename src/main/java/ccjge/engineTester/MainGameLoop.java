package ccjge.engineTester;

import ccjge.entities.Camera;
import ccjge.entities.Entity;
import ccjge.entities.Light;
import ccjge.entities.Player;
import ccjge.guis.GuiRenderer;
import ccjge.guis.GuiTexture;
import ccjge.models.RawModel;
import ccjge.models.TexturedModel;
import ccjge.objConverter.ModelData;
import ccjge.objConverter.OBJFileLoader;
import ccjge.renderEngine.DisplayManager;
import ccjge.renderEngine.Keyboard;
import ccjge.renderEngine.Loader;
import ccjge.renderEngine.MasterRenderer;
import ccjge.terrains.Terrain;
import ccjge.textures.ModelTexture;
import ccjge.textures.TerrainTexture;
import ccjge.textures.TerrainTexturePack;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class MainGameLoop {

    public static void main(String[] args) {
        long window = DisplayManager.createDisplay();
        Loader loader = new Loader();
        MasterRenderer masterRenderer = new MasterRenderer();

        // GUI
        List<GuiTexture> guis = new ArrayList<GuiTexture>();
        GuiTexture gui = new GuiTexture(loader.loadTexture("testTexture"), new Vector2f(0.5f, 0.5f),
                new Vector2f(0.25f, 0.25f));
        guis.add(gui);

        GuiRenderer guiRenderer = new GuiRenderer(loader);

        // Create a light to act as the sun
        Light light = new Light(new Vector3f(3000, 2000, 2000), new Vector3f(1f, 1f, 1f));

        // Create a terrain
        TerrainTexture blendMapTexture = new TerrainTexture(loader.loadTexture("blendMap"));
        TerrainTexture backgroundTerrainTexture = new TerrainTexture(
                loader.loadTexture("grassyTerrainTexture"));
        TerrainTexture dirtTerrainTexture = new TerrainTexture(loader.loadTexture("dirtTerrainTexture"));
        TerrainTexture pinkFlowersTerrainTexture = new TerrainTexture(loader.loadTexture("pinkFlowersTexture"));
        TerrainTexture pathTerrainTexture = new TerrainTexture(loader.loadTexture("pathTerrainTexture"));
        TerrainTexturePack terrainTexturePack = new TerrainTexturePack(backgroundTerrainTexture,
                dirtTerrainTexture, pinkFlowersTerrainTexture, pathTerrainTexture);
        Terrain terrain = new Terrain(0, -1, loader, terrainTexturePack, blendMapTexture, "terrainHeightMap");

        // All entities list
        List<Entity> entities = new ArrayList<Entity>();

        // Dragon player
        ModelData dragonModeData = OBJFileLoader.loadOBJ("dragonModel");
        RawModel dragonModel = loader.loadToVAO(dragonModeData.getVertices(), dragonModeData.getTextureCoords(),
                dragonModeData.getNormals(), dragonModeData.getIndices());
        ModelTexture whiteShineTexture = new ModelTexture(
                loader.loadTexture("whiteTexture"));
        // adding shininess
        whiteShineTexture.setReflectivity(1);
        whiteShineTexture.setShineDamper(10);
        TexturedModel dragonTexturedModel = new TexturedModel(dragonModel, whiteShineTexture);
        Player dragonPlayer = new Player(dragonTexturedModel, new Vector3f(100, 0, -50), 0, 90, 0, 1);

        // Create a camera
        Camera camera = new Camera(dragonPlayer);

        // Grass with transparency
        ModelData grassModelData = OBJFileLoader.loadOBJ("grassModel");
        RawModel grassModel = loader.loadToVAO(grassModelData.getVertices(), grassModelData.getTextureCoords(),
                grassModelData.getNormals(), grassModelData.getIndices());
        ModelTexture grassTexture = new ModelTexture(
                loader.loadTexture("grassTexture"));
        grassTexture.setHasTransparency(true);
        grassTexture.setUseFakeLighting(true);
        TexturedModel grassTexturedModel = new TexturedModel(grassModel, grassTexture);

        // Ferns
        ModelTexture fernTexureAtlas = new ModelTexture(loader.loadTexture("fernTextureAtlas"));
        fernTexureAtlas.setNumberOfRows(2);
        ModelData fernModelData = OBJFileLoader.loadOBJ("fernModel");
        RawModel fernModel = loader.loadToVAO(fernModelData.getVertices(), fernModelData.getTextureCoords(),
                fernModelData.getNormals(), fernModelData.getIndices());
        TexturedModel fernTexturedModel = new TexturedModel(fernModel, fernTexureAtlas);

        Random random = new Random();
        for (int i = 0; i < 500; i++) {

            float x = random.nextFloat() * 800 - 400;
            float z = random.nextFloat() * -600;
            float y = terrain.getHeightOfTerrain(x, z);
            if (i % 2 == 0) {
                entities.add(new Entity(grassTexturedModel,
                        new Vector3f(x, y, z), 0,
                        0, 0, 1));
            }

            if (i % 5 == 0) {
                entities.add(new Entity(fernTexturedModel, random.nextInt(4), new Vector3f(x, y, z), 0,
                        random.nextFloat(), 0, 0.9f));
            }
        }

        entities.add(dragonPlayer);

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!glfwWindowShouldClose(window)) {
            camera.move();
            dragonPlayer.move(terrain);

            masterRenderer.procesTerrain(terrain);
            for (Entity entity : entities) {
                masterRenderer.processEntity(entity);
            }

            // The actual render method
            masterRenderer.render(light, camera);

            // Draw GUI
            guiRenderer.render(guis);

            // Call always at the end
            DisplayManager.updateDisplay(window);

            // check esc key
            if (Keyboard.isKeyDown(GLFW_KEY_ESCAPE)) {
                glfwSetWindowShouldClose(window, true); // close the window
            }

        }

        guiRenderer.cleanUp();
        masterRenderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay(window);
    }
}
