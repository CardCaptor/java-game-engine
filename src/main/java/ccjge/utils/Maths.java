package ccjge.utils;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

import ccjge.entities.Camera;
import ccjge.renderEngine.DisplayManager;

public class Maths {

    public static Matrix4f createTransformationMatrix(Vector2f translation, Vector2f scale) {
        Matrix4f matrix = new Matrix4f();
        matrix.set(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
        Vector3f translation3D = new Vector3f(translation.x, translation.y, 0);
        matrix.translate(translation3D, matrix);
        matrix.scale(new Vector3f(scale.x, scale.y, 1f), matrix);
        return matrix;
    }

    public static Matrix4f createTransformationMatrix(Vector3f translation, float rx, float ry, float rz, float scale) {
        Matrix4f matrix = new Matrix4f();
        matrix.set(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
        matrix.translate(translation, matrix);
        matrix.rotate((float) Math.toRadians(rx), new Vector3f(1, 0, 0), matrix);
        matrix.rotate((float) Math.toRadians(ry), new Vector3f(0, 1, 0), matrix);
        matrix.rotate((float) Math.toRadians(rz), new Vector3f(0, 0, 1), matrix);
        matrix.scale(new Vector3f(scale, scale, scale));
        return matrix;
    }

    public static Matrix4f createProjectionMatrix(float fieldOfView, float nearPlane, float farPlane) {
        float aspectRatio = (float) DisplayManager.getDisplayWidth() / (float) DisplayManager.getDisplayHeight();
        float yScale = (float) ((1f / Math.tan(Math.toRadians(fieldOfView / 2f))) * aspectRatio);
        float xScale = (yScale / aspectRatio);
        float frustrumLength = farPlane - nearPlane;
        Matrix4f projectionMatrix = new Matrix4f();
        projectionMatrix.set(0, 0, xScale);
        projectionMatrix.set(1, 1, yScale);
        projectionMatrix.set(2, 2, -((farPlane + nearPlane) / frustrumLength));
        projectionMatrix.set(2, 3, -1);
        projectionMatrix.set(3, 2, -((2 * farPlane * nearPlane) / frustrumLength));
        projectionMatrix.set(3, 3, 0);
        return projectionMatrix;
    }

    public static Matrix4f createViewMatrix(Camera camera) {
        Matrix4f matrix = new Matrix4f();
        matrix.set(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
        matrix.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), matrix);
        matrix.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), matrix);
        matrix.rotate((float) Math.toRadians(camera.getRoll()), new Vector3f(0, 0, 1), matrix);
        Vector3f cameraPosition = camera.getPosition();
        Vector3f negativeCameraPosition = new Vector3f(-cameraPosition.x, -cameraPosition.y, -cameraPosition.z);
        matrix.translate(negativeCameraPosition, matrix);
        return matrix;
    }

    public static float barryCentric(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos) {
        float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
        float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
        float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
        float l3 = 1.0f - l1 - l2;
        return l1 * p1.y + l2 * p2.y + l3 * p3.y;
    }
}
