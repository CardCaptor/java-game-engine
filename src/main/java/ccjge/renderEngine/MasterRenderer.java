package ccjge.renderEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;

import ccjge.entities.Camera;
import ccjge.entities.Entity;
import ccjge.entities.Light;
import ccjge.models.TexturedModel;
import ccjge.shaders.StaticShader;
import ccjge.shaders.TerrainShader;
import ccjge.terrains.Terrain;
import ccjge.utils.Maths;

public class MasterRenderer {

    // TODO: make this projection configurable
    private static final float FIELD_OF_VIEW = 70;
    private static final float NEAR_PLANE = 0.1f;
    private static final float FAR_PLANE = 1000;

    // TODO: make this configurable
    private static final float SKY_RED = 0.5f;
    private static final float SKY_GREEN = 0.5f;
    private static final float SKY_BLUE = 0.5f;

    private Matrix4f projectionMatrix;

    // entities renderer
    private StaticShader shader = new StaticShader();
    private EntityRenderer renderer;

    // terrain renderer
    private TerrainShader terrainShader = new TerrainShader();
    private TerrainRenderer terrainRenderer;

    // entities list
    private Map<TexturedModel, List<Entity>> entities = new HashMap<TexturedModel, List<Entity>>();

    // terrains
    private List<Terrain> terrains = new ArrayList<Terrain>();

    public static void enableCulling() {
        // Optimize rendering by not rendering back faces
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);
    }

    public static void disableCulling() {
        // Optimize rendering by not rendering back faces
        GL11.glDisable(GL11.GL_CULL_FACE);
    }

    public MasterRenderer() {
        // We enable culling
        enableCulling();

        // We create a projection matrix
        this.projectionMatrix = Maths.createProjectionMatrix(FIELD_OF_VIEW, NEAR_PLANE, FAR_PLANE);

        // we create an entities renderer
        this.renderer = new EntityRenderer(shader, projectionMatrix);

        // we create a terrain renderer
        this.terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);

    }

    public void prepare() {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // clear the framebuffer

        // Sets the sky color
        GL11.glClearColor(SKY_RED, SKY_GREEN, SKY_BLUE, 0.0f);
    }

    public void render(Light sun, Camera camera) {
        prepare();

        // entities
        shader.start();
        shader.loadSkyColor(SKY_RED, SKY_GREEN, SKY_BLUE);
        shader.loadLigth(sun);
        shader.loadViewMatrix(camera);
        renderer.render(entities);
        shader.stop();

        // terrains
        terrainShader.start();
        terrainShader.loadSkyColor(SKY_RED, SKY_GREEN, SKY_BLUE);
        terrainShader.loadLigth(sun);
        terrainShader.loadViewMatrix(camera);
        terrainRenderer.render(terrains);
        terrainShader.stop();

        // clear memory each frame
        terrains.clear();
        entities.clear();
    }

    public void processEntity(Entity entity) {
        TexturedModel texturedModel = entity.getModel();
        List<Entity> batch = entities.get(texturedModel);
        if (batch != null) {
            batch.add(entity);
        } else {
            List<Entity> newBatch = new ArrayList<Entity>();
            newBatch.add(entity);
            entities.put(texturedModel, newBatch);
        }
    }

    public void procesTerrain(Terrain terrain) {
        terrains.add(terrain);
    }

    public void cleanUp() {
        shader.cleanUp();
        terrainShader.cleanUp();
    }
}
