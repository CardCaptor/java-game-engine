package ccjge.renderEngine;

import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import ccjge.entities.Entity;
import ccjge.models.RawModel;
import ccjge.models.TexturedModel;
import ccjge.shaders.StaticShader;
import ccjge.textures.ModelTexture;
import ccjge.utils.Maths;

public class EntityRenderer {

    private StaticShader staticShader;

    public EntityRenderer(StaticShader staticShader, Matrix4f projectionMatrix) {
        this.staticShader = staticShader;
        staticShader.start();
        staticShader.loadProjectionMatrix(projectionMatrix);
        staticShader.stop();
    }

    private void prepareTexturedModel(TexturedModel texturedModel) {
        RawModel rawModel = texturedModel.getRawModel();
        ModelTexture texture = texturedModel.getTexture();

        GL30.glBindVertexArray(rawModel.getVaoId());
        GL20.glEnableVertexAttribArray(0); // This enables the use of VAO 0
        GL20.glEnableVertexAttribArray(1); // This enables the use of VAO 1
        GL20.glEnableVertexAttribArray(2); // This enables the use of VAO 2

        // Checks if the texture is part of an atlas
        staticShader.loadNumberOfRows(texture.getNumberOfRows());

        // Load the reflective lighting of the model
        staticShader.loadShineVariable(texture.getShineDamper(), texture.getReflectivity());

        // We handle render back faces if the texture has transparency
        if (texture.isHasTransparency()) {
            MasterRenderer.disableCulling();
        }
        staticShader.loadUseFakeLighting(texture.isUseFakeLighting());

        // Loads the texture into the shader uniform
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, texturedModel.getTexture().getTextureId());
    }

    private void unbindTexturedModel() {
        MasterRenderer.enableCulling();
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL30.glBindVertexArray(0);
    }

    private void prepareInstance(Entity entity) {
        // Creates a transformation matrix to move the model in the world
        Matrix4f transfomationMatrix = Maths.createTransformationMatrix(entity.getPosition(), entity.getRotX(),
                entity.getRotY(), entity.getRotZ(), entity.getScale());
        // Loads the transformation matrix as a uniform variable in the shader
        staticShader.loadTransformationMatrix(transfomationMatrix);
        // Loads the offset of the texture atlas
        staticShader.loadOffset(entity.getTextureXOffset(), entity.getTextureYOffset());
    }

    public void render(Map<TexturedModel, List<Entity>> entities) {
        for (TexturedModel texturedModel : entities.keySet()) {
            prepareTexturedModel(texturedModel);
            List<Entity> batch = entities.get(texturedModel);
            for (Entity entity : batch) {
                prepareInstance(entity);
                // Draws into the screen
                GL11.glDrawElements(GL11.GL_TRIANGLES, texturedModel.getRawModel().getVertexCount(),
                        GL11.GL_UNSIGNED_INT, 0);
            }
            unbindTexturedModel();
        }
    }

}
