package ccjge.renderEngine;

import java.util.List;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import ccjge.models.RawModel;
import ccjge.shaders.TerrainShader;
import ccjge.terrains.Terrain;
import ccjge.textures.TerrainTexture;
import ccjge.textures.TerrainTexturePack;
import ccjge.utils.Maths;

public class TerrainRenderer {
    private TerrainShader terrainShader;

    public TerrainRenderer(TerrainShader terrainShader, Matrix4f projectionMatrix) {
        this.terrainShader = terrainShader;
        terrainShader.start();
        terrainShader.loadProjectionMatrix(projectionMatrix);
        terrainShader.connectTextureUnits();
        terrainShader.stop();
    }

    private void bindTerrainTextures(Terrain terrain) {
        TerrainTexturePack terrainTexturePack = terrain.getTerrainTexturePack();
        TerrainTexture blendMap = terrain.getBlendMap();

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, terrainTexturePack.getBackgroundTexture().getTextureId());
        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, terrainTexturePack.getrTexture().getTextureId());
        GL13.glActiveTexture(GL13.GL_TEXTURE2);
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, terrainTexturePack.getgTexture().getTextureId());
        GL13.glActiveTexture(GL13.GL_TEXTURE3);
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, terrainTexturePack.getbTexture().getTextureId());
        GL13.glActiveTexture(GL13.GL_TEXTURE4);
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, blendMap.getTextureId());
    }

    private void prepareTerrain(Terrain terrain) {
        RawModel rawModel = terrain.getRawModel();

        GL30.glBindVertexArray(rawModel.getVaoId());
        GL20.glEnableVertexAttribArray(0); // This enables the use of VAO 0
        GL20.glEnableVertexAttribArray(1); // This enables the use of VAO 1
        GL20.glEnableVertexAttribArray(2); // This enables the use of VAO 2

        // Load the reflective lighting of the model
        terrainShader.loadShineVariable(1, 0);

        // Loads the texture into the shader
        bindTerrainTextures(terrain);
    }

    private void unbindTexturedModel() {
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL30.glBindVertexArray(0);
    }

    private void loadModelMatrix(Terrain terrain) {
        // Creates a transformation matrix to move the model in the world
        Matrix4f transfomationMatrix = Maths.createTransformationMatrix(new Vector3f(terrain.getX(), 0, terrain.getZ()),
                0, 0, 0, 1);
        // Loads the transformation matrix as a uniform variable in the shader
        terrainShader.loadTransformationMatrix(transfomationMatrix);
    }

    public void render(List<Terrain> terrains) {
        for (Terrain terrain : terrains) {
            prepareTerrain(terrain);
            loadModelMatrix(terrain);
            // Draws into the screen
            GL11.glDrawElements(GL11.GL_TRIANGLES, terrain.getRawModel().getVertexCount(),
                    GL11.GL_UNSIGNED_INT, 0);
            unbindTexturedModel();
        }
    }

}
