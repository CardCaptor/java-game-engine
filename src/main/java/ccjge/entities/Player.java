package ccjge.entities;

import org.joml.Vector3f;

import ccjge.models.TexturedModel;
import ccjge.renderEngine.DisplayManager;
import ccjge.renderEngine.Keyboard;
import ccjge.terrains.Terrain;

import static org.lwjgl.glfw.GLFW.*;

public class Player extends Entity {

    private static final float RUN_SPEED = 20;
    private static final float TURN_SPEED = 160;
    private static final float GRAVITY = -50;
    private static final float JUMP_POWER = 30;

    private float currentRunSpeed = 0;
    private float currentTurnSpeed = 0;
    private float upwardSpeed = 0;
    private boolean isPlayerInAir = false;

    public Player(TexturedModel texturedModel, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        super(texturedModel, position, rotX, rotY, rotZ, scale);
    }

    public void move(Terrain terrain) {
        checkInputs();
        super.increaseRotation(0, currentTurnSpeed * DisplayManager.getFrameTimeInSeconds(), 0);
        float distance = currentRunSpeed * DisplayManager.getFrameTimeInSeconds();
        float dx = (float) (distance * Math.sin(Math.toRadians(super.getRotY() - 90)));
        float dz = (float) (distance * Math.cos(Math.toRadians(super.getRotY() - 90)));
        super.increasePosition(dx, 0, dz);
        upwardSpeed += GRAVITY * DisplayManager.getFrameTimeInSeconds();
        float terrainHeight = terrain.getHeightOfTerrain(super.getPosition().x, super.getPosition().z);
        if (super.getPosition().y <= terrainHeight) {
            upwardSpeed = 0;
            super.getPosition().y = terrainHeight;
            this.isPlayerInAir = false;
        }
        super.increasePosition(0, upwardSpeed * DisplayManager.getFrameTimeInSeconds(), 0);
    }

    private void checkInputs() {

        if (Keyboard.isKeyDown(GLFW_KEY_W)) {
            this.currentRunSpeed = RUN_SPEED;
        } else if (Keyboard.isKeyDown(GLFW_KEY_S)) {
            this.currentRunSpeed = -RUN_SPEED;
        } else {
            this.currentRunSpeed = 0;
        }

        if (Keyboard.isKeyDown(GLFW_KEY_A)) {
            this.currentTurnSpeed = TURN_SPEED;
        } else if (Keyboard.isKeyDown(GLFW_KEY_D)) {
            this.currentTurnSpeed = -TURN_SPEED;
        } else {
            this.currentTurnSpeed = 0;
        }

        if (Keyboard.isKeyDown(GLFW_KEY_SPACE)) {
            if (!isPlayerInAir) {
                this.upwardSpeed = JUMP_POWER;
                this.isPlayerInAir = true;
            }
        }
    }

}
