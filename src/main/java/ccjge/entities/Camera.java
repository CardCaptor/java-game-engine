package ccjge.entities;

import org.joml.Vector3f;

import ccjge.renderEngine.Mouse;

public class Camera {
    private Vector3f position = new Vector3f(0, 0, 0);
    private float pitch; // how much up or down is the camera
    private float yaw; // how much to the left or right the camera is
    private float roll; // how much the camera is tilted (180 is camera upside down)

    private Player player;

    private float distanceFromPlayer = 50;
    private float angleAroundPlayer = 0;

    public Camera(Player player) {
        this.player = player;
    }

    private void calculateZoom() {
        float zoomLevel = Mouse.getDWheel();
        this.distanceFromPlayer -= zoomLevel;
        Mouse.update(); // resets the scroll event
    }

    private void calculatePitch() {
        if (Mouse.isWheelButtonPressed()) {
            float pitchChange = Mouse.getDY() * 0.1f;
            this.pitch -= pitchChange;
        }
    }

    private void calculateAngleAroundPlayer() {
        if (Mouse.isWheelButtonPressed()) {
            float angleChange = Mouse.getDX() * 0.3f;
            this.angleAroundPlayer -= angleChange;
        }
    }

    private float calculateHorizontalDistanceFromPlayer() {
        return (float) (Math.cos(Math.toRadians(pitch)) * this.distanceFromPlayer);
    }

    private float calculateVerticalDistanceFromPlayer() {
        return (float) (Math.sin(Math.toRadians(pitch)) * this.distanceFromPlayer);
    }

    private void calculateCameraPosition(float horizontalDistance, float verticalDistance) {
        position.y = player.getPosition().y + verticalDistance;
        float cameraRotation = this.player.getRotY() + this.angleAroundPlayer + -90;
        float xOffset = (float) Math.sin(Math.toRadians(cameraRotation))
                * horizontalDistance;
        float zOffset = (float) Math.cos(Math.toRadians(cameraRotation))
                * horizontalDistance;
        position.x = player.getPosition().x - xOffset;
        position.z = player.getPosition().z - zOffset;
        this.yaw = 180 - cameraRotation;
    }

    public void move() {
        calculateZoom();
        calculatePitch();
        calculateAngleAroundPlayer();
        float horizontalDistanceFromPlayer = calculateHorizontalDistanceFromPlayer();
        float verticalDistanceFromPlayer = calculateVerticalDistanceFromPlayer();
        calculateCameraPosition(horizontalDistanceFromPlayer, verticalDistanceFromPlayer);
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }

}
