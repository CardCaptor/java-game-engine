package ccjge;

public class Constants {
    public static final String RESOURCES_FOLDER_PATH = "src/main/java/ccjge/resources/";
    public static final String OBJ_FILE_EXTENSION = ".obj";
    public static final String PNG_FILE_EXTENSION = ".png";
}
