package ccjge.shaders;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import ccjge.entities.Camera;
import ccjge.entities.Light;
import ccjge.utils.Maths;

public class TerrainShader extends ShaderProgram {

    private static final String VERTEX_SHADER_FILE = "src/main/java/ccjge/shaders/terrainVertexShader.vs";
    private static final String FRAGMENT_SHADER_FILE = "src/main/java/ccjge/shaders/terrainFragmentShader.fs";

    private int transformationMatrixLocation;
    private int projectionMatrixLocation;
    private int viewMatrixLocation;
    private int lightPositionLocation;
    private int lightColorLocation;
    private int shineDamperLocation;
    private int reflectivityLocation;
    private int skyColorLocation;
    private int backgroundTextureLocation;
    private int rTextureLocation;
    private int gTextureLocation;
    private int bTextureLocation;
    private int blendMapLocation;

    public TerrainShader() {
        super(VERTEX_SHADER_FILE, FRAGMENT_SHADER_FILE);
    }

    @Override
    protected void bindAttributes() {
        // Remember when naming that this is per vertex
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
        super.bindAttribute(2, "normal");
    }

    @Override
    protected void getAllUniformLocations() {
        transformationMatrixLocation = super.getUniformLocation("transformationMatrix");
        projectionMatrixLocation = super.getUniformLocation("projectionMatrix");
        viewMatrixLocation = super.getUniformLocation("viewMatrix");
        lightPositionLocation = super.getUniformLocation("lightPosition");
        lightColorLocation = super.getUniformLocation("lightColor");
        shineDamperLocation = super.getUniformLocation("shineDamper");
        reflectivityLocation = super.getUniformLocation("reflectivity");
        skyColorLocation = super.getUniformLocation("skyColor");
        backgroundTextureLocation = super.getUniformLocation("backgroundTexture");
        rTextureLocation = super.getUniformLocation("rTexture");
        gTextureLocation = super.getUniformLocation("gTexture");
        bTextureLocation = super.getUniformLocation("bTexture");
        blendMapLocation = super.getUniformLocation("blendMap");
    }

    public void loadTransformationMatrix(Matrix4f matrix) {
        super.loadMatrix(transformationMatrixLocation, matrix);
    }

    public void loadProjectionMatrix(Matrix4f matrix) {
        super.loadMatrix(projectionMatrixLocation, matrix);
    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        super.loadMatrix(viewMatrixLocation, matrix);
    }

    public void loadLigth(Light light) {
        super.loadVector(lightPositionLocation, light.getPosition());
        super.loadVector(lightColorLocation, light.getColor());
    }

    public void loadShineVariable(float shineDamper, float reflectivity) {
        super.loadFloat(shineDamperLocation, shineDamper);
        super.loadFloat(reflectivityLocation, reflectivity);
    }

    public void loadSkyColor(float r, float g, float b) {
        super.loadVector(skyColorLocation, new Vector3f(r, g, b));
    }

    public void connectTextureUnits() {
        super.loadInt(backgroundTextureLocation, 0);
        super.loadInt(rTextureLocation, 1);
        super.loadInt(gTextureLocation, 2);
        super.loadInt(bTextureLocation, 3);
        super.loadInt(blendMapLocation, 4);

    }
}
