#version 400 core

in vec3 position;
in vec2 textureCoords;
in vec3 normal;

// This matrix contains information about the translation, rotation and scale of the object
uniform mat4 transformationMatrix; 
// this matrix creates perspective, so we can look at 3d objects in a 2d screen
uniform mat4 projectionMatrix;
// since there is no "camera", to simulate camera movement we move the entire world in the oposite direction, 
// this matrix contains the inverse of the camera position
uniform mat4 viewMatrix; 
uniform vec3 lightPosition;

float fogGradient = 1.5;
float fogDensity = 0.007;

out vec2 textureCoordinates;
out vec3 surfaceNormal;
out vec3 toLightVector;
out vec3 toCameraVector;
out float visibility;

void main(void){
    // We call world position the original position of the object, 
    // after we applied translation, rotation and scale, 
    // so this new vector represents the real position of the object in the world
    vec4 worldPosition = transformationMatrix * vec4(position,1.0);
    vec4 relativeToCameraPosition = viewMatrix * worldPosition;
    gl_Position = projectionMatrix * relativeToCameraPosition;
    textureCoordinates = textureCoords;
    surfaceNormal = (transformationMatrix * vec4(normal,0.0)).xyz;
    toLightVector = lightPosition - worldPosition.xyz;
    toCameraVector = (inverse(viewMatrix) * vec4(0.0,0.0,0.0,1.0)).xyz - worldPosition.xyz;

    // Visibility calculation to simulate fog
    float relativeToCameraDistance = length(relativeToCameraPosition.xyz);
    visibility = exp(-pow((relativeToCameraDistance*fogDensity),fogGradient));
    visibility = clamp(visibility,0.0,1.0);
}