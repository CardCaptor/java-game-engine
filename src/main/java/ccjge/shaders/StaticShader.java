package ccjge.shaders;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

import ccjge.entities.Camera;
import ccjge.entities.Light;
import ccjge.utils.Maths;

public class StaticShader extends ShaderProgram {

    private static final String VERTEX_SHADER_FILE = "src/main/java/ccjge/shaders/vertexShader.vs";
    private static final String FRAGMENT_SHADER_FILE = "src/main/java/ccjge/shaders/fragmentShader.fs";

    private int transformationMatrixLocation;
    private int projectionMatrixLocation;
    private int viewMatrixLocation;
    private int lightPositionLocation;
    private int lightColorLocation;
    private int shineDamperLocation;
    private int reflectivityLocation;
    private int useFakeLightingLocation;
    private int skyColorLocation;
    private int numberOfRowsLocation;
    private int offsetLocation;

    public StaticShader() {
        super(VERTEX_SHADER_FILE, FRAGMENT_SHADER_FILE);
    }

    @Override
    protected void bindAttributes() {
        // Remember when naming that this is per vertex
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
        super.bindAttribute(2, "normal");
    }

    @Override
    protected void getAllUniformLocations() {
        transformationMatrixLocation = super.getUniformLocation("transformationMatrix");
        projectionMatrixLocation = super.getUniformLocation("projectionMatrix");
        viewMatrixLocation = super.getUniformLocation("viewMatrix");
        lightPositionLocation = super.getUniformLocation("lightPosition");
        lightColorLocation = super.getUniformLocation("lightColor");
        shineDamperLocation = super.getUniformLocation("shineDamper");
        reflectivityLocation = super.getUniformLocation("reflectivity");
        useFakeLightingLocation = super.getUniformLocation("useFakeLighting");
        skyColorLocation = super.getUniformLocation("skyColor");
        numberOfRowsLocation = super.getUniformLocation("numberOfRows");
        offsetLocation = super.getUniformLocation("offset");
    }

    public void loadTransformationMatrix(Matrix4f matrix) {
        super.loadMatrix(transformationMatrixLocation, matrix);
    }

    public void loadProjectionMatrix(Matrix4f matrix) {
        super.loadMatrix(projectionMatrixLocation, matrix);
    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        super.loadMatrix(viewMatrixLocation, matrix);
    }

    public void loadLigth(Light light) {
        super.loadVector(lightPositionLocation, light.getPosition());
        super.loadVector(lightColorLocation, light.getColor());
    }

    public void loadShineVariable(float shineDamper, float reflectivity) {
        super.loadFloat(shineDamperLocation, shineDamper);
        super.loadFloat(reflectivityLocation, reflectivity);
    }

    public void loadUseFakeLighting(boolean useFakeLighting) {
        super.loadBoolean(useFakeLightingLocation, useFakeLighting);
    }

    public void loadSkyColor(float r, float g, float b) {
        super.loadVector(skyColorLocation, new Vector3f(r, g, b));
    }

    public void loadNumberOfRows(int numberOfRows) {
        super.loadInt(numberOfRowsLocation, numberOfRows);
    }

    public void loadOffset(float x, float y) {
        super.loadVector2D(offsetLocation, new Vector2f(x, y));
    }

}
