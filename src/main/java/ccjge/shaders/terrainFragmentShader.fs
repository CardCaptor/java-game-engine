#version 400 core

in vec2 textureCoordinates;
in vec3 surfaceNormal;
in vec3 toLightVector;
in vec3 toCameraVector;
in float visibility;

out vec4 outColor;

uniform sampler2D backgroundTexture;
uniform sampler2D rTexture;
uniform sampler2D gTexture;
uniform sampler2D bTexture;
uniform sampler2D blendMap;
uniform vec3 lightColor;
uniform float shineDamper;
uniform float reflectivity;
uniform vec3 skyColor;

void main(void){
    vec3 unitSurfaceNormal = normalize(surfaceNormal); // normal vector
    vec3 unitToLightVector = normalize(toLightVector); // vector pointing to the light source
    vec3 unitToCameraVector = normalize(toCameraVector); // vector pointing to the camera
    vec3 unitLightDirectionVector = -unitToLightVector; // vector pointing from the light source to the vertex
    vec3 unitLightReflectionVector = reflect(unitLightDirectionVector, unitSurfaceNormal); // vector containing the reflection of the light with the normal

    // diffuse light calculation
    // we calculate how close to the normal the vector pointing to the light is
    // the closer they are, the more bright the pixel is
    float nDot1 = dot(unitSurfaceNormal,unitToLightVector);
    float brightness = max(nDot1,0.0);
    vec3 diffuse = brightness * lightColor;
    
    // reflective light calculation
    // we calculate how close is the vector pointing to the camera with the reflected light vector
    // the closer they are, the more light is "entering" into the camera
    float specularFactor = dot(unitToCameraVector,unitLightReflectionVector);
    specularFactor = max(specularFactor,0.2); // 0.2 instead of 0 adds ambient lighting, making sure the pixel is never completely black
    // we damp it (we make lower values even lower)
    float dampedFactor = pow(specularFactor,shineDamper);
    // we take into account how reflective is the object
    vec3 finalSpecular = dampedFactor * reflectivity  * lightColor;

    // We calculate the color of the pixel based on textures
    // We use the background texture as a fallback (black in the blendMap)
    // And we sample 3 other textures and connect them to the r,g,b channels of the blendMap
    vec4 blendMapColorSample = texture(blendMap,textureCoordinates);
    // We calculate how much black is in the blend map to render the background texture
    float blackColorAmount = 1 - (blendMapColorSample.r + blendMapColorSample.g + blendMapColorSample.b);
    vec2 tiledTextureCoordinates = textureCoordinates * 40;
    vec4 backgroundColorSample = texture(backgroundTexture,tiledTextureCoordinates) * blackColorAmount;
    vec4 rColorSample = texture(rTexture,tiledTextureCoordinates) * blendMapColorSample.r;
    vec4 gColorSample = texture(gTexture,tiledTextureCoordinates) * blendMapColorSample.g;
    vec4 bColorSample = texture(bTexture,tiledTextureCoordinates) * blendMapColorSample.b;
    vec4 finalColor = backgroundColorSample + rColorSample + gColorSample + bColorSample; 


    // final color of the pixel
    outColor = vec4(diffuse,1.0) * finalColor + vec4(finalSpecular,1.0);
    // We add simple fog simulation
    outColor = mix(vec4(skyColor,1.0),outColor,visibility);

}
